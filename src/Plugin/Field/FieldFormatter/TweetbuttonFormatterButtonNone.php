<?php /**
 * @file
 * Contains \Drupal\tweetbutton\Plugin\Field\FieldFormatter\TweetbuttonFormatterButtonNone.
 */

namespace Drupal\tweetbutton\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;

/**
 * @FieldFormatter(
 *  id = "tweetbutton_formatter_button_none",
 *  label = @Translation("Tweetbutton style none"),
 *  field_types = {"tweetbutton"}
 * )
 */
class TweetbuttonFormatterButtonNone extends FormatterBase {

  /**
   * @FIXME
   * Move all logic relating to the tweetbutton_formatter_button_none formatter into this
   * class. For more information, see:
   *
   * https://www.drupal.org/node/1805846
   * https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Field%21FormatterInterface.php/interface/FormatterInterface/8
   * https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Field%21FormatterBase.php/class/FormatterBase/8
   */

}
