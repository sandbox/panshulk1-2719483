<?php /**
 * @file
 * Contains \Drupal\tweetbutton\Plugin\Field\FieldWidget\Tweetbutton.
 */

namespace Drupal\tweetbutton\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;

/**
 * @FieldWidget(
 *  id = "tweetbutton",
 *  label = @Translation("Tweetbutton"),
 *  field_types = {"tweetbutton"}
 * )
 */
class Tweetbutton extends WidgetBase {

  /**
   * @FIXME
   * Move all logic relating to the tweetbutton widget into this class.
   * For more information, see:
   *
   * https://www.drupal.org/node/1796000
   * https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Field%21WidgetInterface.php/interface/WidgetInterface/8
   * https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Field%21WidgetBase.php/class/WidgetBase/8
   */

}
