<?php

/**
 * @file
 * Contains \Drupal\tweetbutton\Form\TweetbuttonAdminSettings.
 */

namespace Drupal\tweetbutton\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class TweetbuttonAdminSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tweetbutton_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['tweetbutton.settings'];
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $form = [];
    $config = \Drupal::config('tweetbutton.settings');
    $form['button'] = [
      '#type' => 'fieldset',
      '#title' => t('Default settings for tweetbutton'),
    ];

    $form['button']['tweetbutton_button'] = [
      '#type' => 'select',
      '#options' => [
        'vertical' => t('Vertical Count'),
        'horizontal' => t('Horizontal Count'),
        'none' => t('No count'),
      ],
      '#default_value' => $config->get('tweetbutton_button'),
      '#id' => 'tweetbutton-button',
    ];

    $form['button']['tweetbutton_tweet_text'] = [
      '#type' => 'textfield',
      '#title' => t('Tweet Text'),
      '#default_value' => $config->get('tweetbutton_tweet_text'),
      '#description' => t('Tweet text to use as a default text, if no values are passed, leave this to blank to use page title as tweet text.'),
    ];

    $form['button']['tokens'] = [
      '#token_types' => ['node'],
      '#theme' => 'token_tree_link',
    ];

    $form['button']['tweetbutton_size'] = [
      '#title' => t('Tweetbutton size'),
      '#type' => 'select',
      '#options' => [
        'medium' => t('Medium'),
        'large' => t('Large'),
      ],
      '#default_value' => $config->get('tweetbutton_size'),
    ];

    $form['button']['tweetbutton_hashtags'] = [
      '#title' => t('Hashtags'),
      '#type' => 'textfield',
      '#default_value' => $config->get('tweetbutton_hashtags'),
      '#description' => t('Comma separated hashtags to be used in every tweet'),
    ];


    $form['button']['tweetbutton_language'] = [
      '#title' => t('Language'),
      '#description' => t('This is the language that the button will render in on your website. People will see the Tweet dialog in their selected language for Twitter.com.'),
      '#type' => 'select',
      '#options' => [
        'en' => t('English'),
        'fr' => t('French'),
        'de' => t('German'),
        'es' => t('Spanish'),
        'ja' => t('Japanese'),
        'auto' => t('Automatic'),
      ],
      '#default_value' => $config->get('tweetbutton_language'),
    ];

    if (\Drupal::moduleHandler()->moduleExists('shorten')) {

      $services = [];
      $services[0] = t('Use t.co twitter default url shortener');
      $all_services = \Drupal::moduleHandler()->invokeAll('shorten_service');
      foreach (array_keys($all_services) as $value) {
        $services[$value] = $value;
      }

      $form['button']['tweetbutton_shorten_service'] = [
        '#title' => t('Shorten service to use to add custom url'),
        '#type' => 'select',
        '#options' => $services,
        '#default_value' => $config->get('tweetbutton_shorten_service'),
      ];
    }

    $form['button']['follow'] = [
      '#type' => 'fieldset',
      '#title' => t('Recommend people to follow'),
    ];

    $form['button']['follow']['tweetbutton_account'] = [
      '#type' => 'textfield',
      '#title' => t('Twitter account to follow'),
      '#description' => t('This user will be @mentioned in the suggested. Will be used as default if tweetbutton fields author twitter account is not set'),
      '#default_value' => $config->get('tweetbutton_account'),
      '#id' => 'tweetbutton-account',
    ];

    $form['button']['follow']['tweetbutton_rel_account_name'] = [
      '#type' => 'textfield',
      '#title' => t('Related Account'),
      '#default_value' => $config->get('tweetbutton_rel_account_name'),
      '#description' => t('This should be site default twitter account'),
    ];

    $form['button']['follow']['tweetbutton_rel_account_description'] = [
      '#type' => 'textfield',
      '#title' => t('Related Account Description'),
      '#default_value' => $config->get('tweetbutton_rel_account_description'),
    ];

    $form['follow_button'] = [
      '#type' => 'fieldset',
      '#title' => t('Follow button settings'),
    ];

    $form['follow_button']['tweetbutton_follow_show_count'] = [
      '#type' => 'checkbox',
      '#title' => t('Show follow count'),
      '#default_value' => $config->get('tweetbutton_follow_show_count'),
    ];

    $form['follow_button']['tweetbutton_follow_screen_name'] = [
      '#type' => 'textfield',
      '#title' => t('Screen name to follow'),
      '#default_value' => $config->get('tweetbutton_follow_screen_name'),
    ];

    $form['follow_button']['tweetbutton_follow_size'] = [
      '#title' => t('Tweetbutton size'),
      '#type' => 'select',
      '#options' => [
        'medium' => t('Medium'),
        'large' => t('Large'),
      ],
      '#default_value' => $config->get('tweetbutton_follow_size'),
    ];

    return parent::buildForm($form, $form_state);
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('tweetbutton.settings');

    $config->set('tweetbutton_button', $form_state->getValue('tweetbutton_button'))
      ->set('tweetbutton_tweet_text', $form_state->getValue('tweetbutton_tweet_text'))
      ->set('tweetbutton_size', $form_state->getValue('tweetbutton_size'))
      ->set('tweetbutton_hashtags', $form_state->getValue('tweetbutton_hashtags'))
      ->set('tweetbutton_language', $form_state->getValue('tweetbutton_language'))
      ->set('tweetbutton_shorten_service', $form_state->getValue('tweetbutton_shorten_service'))
      ->set('tweetbutton_account', $form_state->getValue('tweetbutton_account'))
      ->set('tweetbutton_rel_account_name', $form_state->getValue('tweetbutton_rel_account_name'))
      ->set('tweetbutton_rel_account_description', $form_state->getValue('tweetbutton_rel_account_description'))
      ->set('tweetbutton_follow_show_count', $form_state->getValue('tweetbutton_follow_show_count'))
      ->set('tweetbutton_follow_screen_name', $form_state->getValue('tweetbutton_follow_screen_name'))
      ->set('tweetbutton_follow_size', $form_state->getValue('tweetbutton_follow_size'))->save();

    parent::submitForm($form, $form_state);
  }

}
